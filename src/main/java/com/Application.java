package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.repository.AsignaturaRepository;
import com.tasks.GestorCorreo;

@SpringBootApplication
public class Application  extends SpringBootServletInitializer  {


	@Autowired
	private GestorCorreo gestor;
	@Autowired
	private AsignaturaRepository asignaturaRepository;
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	public void run(String... args) throws Exception {
	this.gestor.sendSimpleMessage("arturopardosanchez@gmail.com", "lorem","Desde main");
//		for(int i = 1; i<= 10; i++) {
//			Asignatura a = new Asignatura();
//			a.setNombre("asignatura "+i);
//			a.setSiglas("ASIG "+i);
//			a.setHorasLectivas((100+i));
//			this.asignaturaRepository.save(a);
//		}
	}


}
