package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Alumno;
import com.entity.Colegio;
import com.entity.Profesor;
import com.entity.UserOld;


@Repository
public interface ProfesorRepository extends JpaRepository<Profesor, Long> {

}
