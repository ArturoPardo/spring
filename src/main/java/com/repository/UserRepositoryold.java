package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.UserOld;


@Repository
public interface UserRepositoryold extends JpaRepository<UserOld, Long> {

}
