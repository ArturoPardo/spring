package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Colegio;
import com.entity.UserOld;


@Repository
public interface ColegioRepository extends JpaRepository<Colegio, Long> {

}
