package com.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entity.Alumno;



@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Long> {}
