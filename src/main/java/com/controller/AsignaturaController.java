package com.controller;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.Alumno;
import com.entity.Asignatura;
import com.repository.AsignaturaRepository;


@Controller
@RequestMapping("/asignatura")
public class AsignaturaController {
	

	@Autowired
	private AsignaturaRepository asignaturaRepository;
	
	@GetMapping("/signupasignatura")
    public String showSignUpForm(Asignatura asignatura) {
        return "add-asignatura";
    }
	@GetMapping("/viewasignatura")
    public String showViewAsignatura(Model model) {
        model.addAttribute("asignaturas", this.asignaturaRepository.findAll());
        return "ver-asignatura";
    }
     
    @PostMapping("/addasignatura")
    public String addUser(@Valid Asignatura asignatura, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-asignatura";
        }
         
        this.asignaturaRepository.save(asignatura);
        model.addAttribute("asignaturas", this.asignaturaRepository.findAll());
        return "ver-asignatura";
    }
 
    // additional CRUD methods
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Asignatura asignatura = this.asignaturaRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid asignatura Id:" + id));
         
        model.addAttribute("asignatura", asignatura);
        return "update-asignatura";
    }
    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid Asignatura asignatura, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            asignatura.setId(id);
            return "update-asignatura";
        }
             
        this.asignaturaRepository.save(asignatura);
        model.addAttribute("asignaturas", this.asignaturaRepository.findAll());
        return "index";
    }
         
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        Asignatura asignatura = this.asignaturaRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid asignatura Id:" + id));
        asignaturaRepository.delete(asignatura);
        model.addAttribute("asignaturas", this.asignaturaRepository.findAll());
        return "redirect:/asignatura/viewasignatura";
    }
    
}
