package com.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.service.ColegioService;


@Controller
public class ViewController {


	@Autowired
	private ColegioService colegioService;

	@GetMapping("/enviar")
	public String enviar() {
		this.colegioService.enviar();
		   return "redirect:/alumno/formularioFaltas";
	}

	




	// additional CRUD methods
}