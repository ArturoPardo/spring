package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Colegio;
import com.service.ColegioService;

@RestController
@RequestMapping("/colegio")
public class ColegioController {
	@Autowired
	private ColegioService colegioService;
	@PostMapping

	public void crear(@RequestBody Colegio colegio) {
		this.colegioService.crearColegio(colegio);
	}
	@GetMapping
	public List<Colegio>getAll(){
		return this.colegioService.getAllColegios();
	}
}
