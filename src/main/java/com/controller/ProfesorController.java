package com.controller;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.Profesor;
import com.repository.ProfesorRepository;


@Controller
@RequestMapping("/profesor")
public class ProfesorController {
	

	@Autowired
	private ProfesorRepository profesorRepository;
	
	@GetMapping("/signupprofesor")
    public String showSignUpForm(Profesor profesor) {

        return "add-profesor";
    }
	@GetMapping("/viewprofesor")
    public String showViewProfesor(Model model) {
		  model.addAttribute("profesores", this.profesorRepository.findAll());
        return "ver-profesor";
    }
     
    @PostMapping("/addprofesor")
    public String addUser(@Valid Profesor profesor, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-profesor";
        }
         
        this.profesorRepository.save(profesor);
        model.addAttribute("profesores", this.profesorRepository.findAll());
        return "ver-profesor";
    }
 
    // additional CRUD methods
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Profesor profesor = this.profesorRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid profesor Id:" + id));
         
        model.addAttribute("profesor", profesor);
        return "update-profesor";
    }
    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid Profesor profesor, 
      BindingResult result, Model model) {
        if (result.hasErrors()) {
            profesor.setId(id);
            return "update-profesor";
        }
             
        this.profesorRepository.save(profesor);
        model.addAttribute("profesores", this.profesorRepository.findAll());
        return "index";
    }
         
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        Profesor profesor = this.profesorRepository.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid profesor Id:" + id));
        profesorRepository.delete(profesor);
        model.addAttribute("profesores", this.profesorRepository.findAll());
        return "redirect:/profesor/viewprofesor";
    }
}
