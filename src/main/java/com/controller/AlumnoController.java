package com.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.entity.Alumno;
import com.entity.Asignatura;
import com.pojo.FormularioAlumnoAsignatura;
import com.pojo.FormularioFaltaAsistencia;
import com.repository.AlumnoRepository;
import com.repository.AsignaturaRepository;
import com.service.AlumnoService;

@Controller
@RequestMapping("/alumno")
public class AlumnoController {

	@Autowired
	private AlumnoRepository alumnoRepository;

	// falta
	@Autowired
	private AlumnoService alumnoService;
	@Autowired
	private AsignaturaRepository asignaturaRepository;

	@GetMapping("/formularioFaltas")
	public String formularioFaltas(Model model) {
		model.addAttribute("falta", new FormularioFaltaAsistencia());
		return "add-falta";
	}

	@PostMapping("/addfalta")
	public String formularioFaltas(@Valid FormularioFaltaAsistencia falta, BindingResult result, Model model) {
		this.alumnoService.crearFalta(falta);
		model.addAttribute("falta", new FormularioFaltaAsistencia());
		return "add-falta";
	}

	// falta
	@GetMapping("/signupalumno")
	public String showSignUpForm(Alumno alumno) {
		return "add-alumno";
	}

	@GetMapping("/viewalumno")
	public String showViewAlumno(Model model) {
		model.addAttribute("alumnos", this.alumnoRepository.findAll());
		return "ver-alumno";
	}

	@PostMapping("/addalumno")
	public String addUser(@Valid Alumno alumno, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-alumno";
		}

		this.alumnoRepository.save(alumno);
		model.addAttribute("alumnos", this.alumnoRepository.findAll());
		return "ver-alumno";
	}

	@GetMapping("/vincular/{id}")
	public String vincularUser(@PathVariable("id") long id, Model model) {
		Alumno alumno = this.alumnoRepository.findById(id)
		.orElseThrow(() -> new IllegalArgumentException("Invalid alumno Id:" + id));

		model.addAttribute("alumno", alumno);
		model.addAttribute("pojo", new FormularioAlumnoAsignatura());

		model.addAttribute("asignaturas", this.asignaturaRepository.findAll());
		return "vincular-alumno";
	}

	@PostMapping("/vincularalumno/{id}")
	public String vincularAlumnoAsignatura(@Valid FormularioAlumnoAsignatura pojo,@PathVariable("id") long id, BindingResult result, Model model) {
		
	Alumno a = this.alumnoRepository.findById(id).orElse(null);
	Asignatura as = this.asignaturaRepository.findById(pojo.getIdAsignatura()).orElse(null);
	System.out.println("");
		a.getAsignaturas().add(as);
	
		
		
	
		this.alumnoRepository.save(a);

		//this.alumnoRepository.save(alumno);
		model.addAttribute("alumnos", this.alumnoRepository.findAll());
		return "ver-alumno";
	}

	// additional CRUD methods
	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Alumno alumno = this.alumnoRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid alumno Id:" + id));

		model.addAttribute("alumno", alumno);
		return "update-alumno";
	}

	@PostMapping("/update/{id}")
	public String updateUser(@PathVariable("id") long id, @Valid Alumno alumno, BindingResult result, Model model) {
		if (result.hasErrors()) {
			alumno.setId(id);
			return "update-alumno";
		}

		this.alumnoRepository.save(alumno);
		model.addAttribute("alumnos", this.alumnoRepository.findAll());
		return "index";
	}

	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") long id, Model model) {
		Alumno alumno = this.alumnoRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid alumno Id:" + id));
		alumnoRepository.delete(alumno);
		model.addAttribute("alumnos", this.alumnoRepository.findAll());
		return "redirect:/alumno/viewalumno";
	}

}
