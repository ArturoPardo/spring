package com.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Alumno;
import com.entity.Asignatura;
import com.pojo.FormularioAlumnoAsignatura;
import com.pojo.FormularioFaltaAsistencia;
import com.repository.AlumnoRepository;
import com.repository.AsignaturaRepository;
import com.service.AlumnoService;

@RestController
@RequestMapping("/alumno")
public class RestAlumnoController {

	@Autowired
	private AlumnoRepository alumnoRepository;

	// falta
	@Autowired
	private AlumnoService alumnoService;

	@GetMapping("/viewalumno")
	public List<Alumno> showViewAlumno() {
		return this.alumnoRepository.findAll();
	}

}
