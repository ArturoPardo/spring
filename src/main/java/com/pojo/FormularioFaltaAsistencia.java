package com.pojo;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Data
public class FormularioFaltaAsistencia implements Serializable {

	private static final long serialVersionUID = 6050240296854845785L;



	private Long idAlumno;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;
	
}
