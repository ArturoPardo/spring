package com.pojo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormularioAlumnoAsignatura implements Serializable {

	private static final long serialVersionUID = 6050240296854845785L;

	private Long idAlumno;
	private Long idAsignatura;
}
