package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Alumno;
import com.entity.Colegio;
import com.entity.Profesor;
import com.repository.AlumnoRepository;
import com.repository.ColegioRepository;
import com.repository.ProfesorRepository;

@Service
public class ProfesorService {

	@Autowired
  private ProfesorRepository profesorRepository;
	
	public void crearProfesor(Profesor profesor) {
		
		this.profesorRepository.save(profesor);
	}
	public  List<Profesor> getAllProfesores(){
		return this.profesorRepository.findAll();
	}
}
