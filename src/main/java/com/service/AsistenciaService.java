package com.service;

import com.entity.FaltaAsistencia;

public interface AsistenciaService {

	FaltaAsistencia findById(Long id);

	FaltaAsistencia saveOrUpdate(FaltaAsistencia parametrizacionPrograma);
}
