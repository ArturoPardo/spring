package com.service;

import com.entity.UserOld;

public interface UserService {

	UserOld findById(Long id);

	UserOld saveOrUpdate(UserOld parametrizacionPrograma);
}
