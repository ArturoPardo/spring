package com.service;

import java.util.List;

import com.entity.Asignatura;

public interface AsignaturaService {

	Asignatura findById(Long id);

	Asignatura saveOrUpdate(Asignatura parametrizacionPrograma);

	List<Asignatura> devolverAsignaturas();
}
