package com.service;

import org.springframework.stereotype.Repository;

import com.entity.Alumno;
import com.pojo.FormularioFaltaAsistencia;

public interface AlumnoService {

	Alumno findById(Long id);

	Alumno saveOrUpdate(Alumno parametrizacionPrograma);
	
	void crearFalta(FormularioFaltaAsistencia falta);
}
