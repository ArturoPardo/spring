package com.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entity.UserOld;
import com.repository.UserRepositoryold;
import com.service.UserService;

import exception.NotFoundException;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserRepositoryold userRepository;

	@Override
	public UserOld findById(final Long id) {
		LOGGER.debug("Getting User with id {}", id);
		Optional<UserOld> user = userRepository.findById(id);
		if (!user.isPresent()) {
			LOGGER.error("User with id {} not found", id);
			throw new NotFoundException("User con id " + id + " no encontrada");
		}
		return user.get();
	}

	@Override
	@Transactional(readOnly = false)
	public UserOld saveOrUpdate(UserOld user) {
		if (user.getId() != null) {
			LOGGER.debug("User {} with id {}", user, user.getId());
			this.findById(user.getId());
		} else {
			LOGGER.debug("Saving new User {}", user);
		}
		return this.userRepository.save(user);
	}

}
