package com.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Alumno;
import com.entity.FaltaAsistencia;
import com.pojo.FormularioFaltaAsistencia;
import com.repository.AlumnoRepository;
import com.repository.FaltaAsistenciaRepository;
import com.service.AlumnoService;

@Service
public class AlumnoServiceImpl implements AlumnoService {

	@Autowired
	private AlumnoRepository alumnoRepository;
	
	@Autowired
	private FaltaAsistenciaRepository faltaRepository;
	
	@Override
	public Alumno findById(Long id) {
		return null;
	}

	@Override
	public void crearFalta(FormularioFaltaAsistencia faltaPojo) {

		FaltaAsistencia faltaEntidad = new FaltaAsistencia();
		Alumno alumno = this.alumnoRepository.findById(faltaPojo.getIdAlumno()).orElse(null);
		
		faltaEntidad.setFecha(faltaPojo.getFecha());
		faltaEntidad.setAlumno(alumno);
		
		this.faltaRepository.save(faltaEntidad);
		
	}

	@Override
	public Alumno saveOrUpdate(Alumno parametrizacionPrograma) {
		// TODO Auto-generated method stub
		return null;
	}

}

