package com.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity 
@NoArgsConstructor
@Data 
public class UserOld implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3448482279811920813L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String name;

    private String email;

    
    
}

