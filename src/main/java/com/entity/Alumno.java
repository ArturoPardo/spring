package com.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import enums.Sexo;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Alumno implements Serializable {

	private static final long serialVersionUID = -7279280281290036969L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String nombre;
	private String apellidos;
	private String dni;
	private int edad;
	@Enumerated(EnumType.STRING)
	private Sexo sexo;
	@ManyToMany
	@JoinTable(
	  name = "alumno_asignatura", 
	  joinColumns = @JoinColumn(name = "id_alumno"), 
	  inverseJoinColumns = @JoinColumn(name = "id_asignatura"))
	List<Asignatura> asignaturas;
}
