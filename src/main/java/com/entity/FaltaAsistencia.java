package com.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;


import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class FaltaAsistencia implements Serializable {

	private static final long serialVersionUID = -7279280281290036969L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	private Alumno alumno;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;
	 @Column ( columnDefinition = "boolean default false")
	private Boolean enviado;

}
